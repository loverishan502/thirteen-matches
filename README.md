**Le jeu des 13 allumettes**

# Règles:
Le jeu des 13 allumettes se joue à deux joueurs qui, à tour de rôle, doivent prendre 1, 2 ou 3
allumettes d’un tas qui en contient initialement 13. Le perdant est le joueur qui prend la dernière
allumette.

# plusieurs niveaux :
* (n)aïf        c'est le hasard qui détermine le nombre d’allumettes que prend la machine
* (d)istrait    le nb d'allumettes est choisi au hasard entre 1 et 3 sans tenir compte de celles dans le tas
* (r)apide      le plus grand nb possible d'allumettes est pris à chaque tour de jeu (soit 3)
* (e)xpert      la machine joue le mieux possible

En début de jeu, la machine demande au joueur humain son nom puis le niveau de l’ordinateur (par défaut expert)
La machine affiche le niveau choisi.<br>
Ensuite, la machine demande au joueur humain s’il veut commencer ou non.<br>
Le joueur humain commence si et seulement s’il répond 'o' ou 'O'.<br>
Si un joueur essaie de tricher, son coup est annulé et il doit recommencer.

# Pre-requis:
* python3

# Lancement du jeu sur un unix
python3 13allumettes.py
