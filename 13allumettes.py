from random import randint

# Création de la liste qui contient les 13 allumettes
allumettes = []
for i in range(3):
    allumettes.append(['|', '|', '|', '|', '|  ', '|', '|', '|', '|', '|  ', '|', '|', '|'])

# Fonction qui permet d'afficher les allumettes restantes à tout moment
def affiche_ecran(suite):
    for ligne in suite:
        print(" ".join(ligne))

# Fonction qui permet de retirer les allumettes en fonction du nombre choisi
def retrait_allumettes(nb_allum):
    for x in range(3):
        try:
            if nb_allum == 1:
                del allumettes[x][-1]
            elif nb_allum == 2:
                del allumettes[x][-3:-1]
            elif nb_allum == 3:
                del allumettes[x][-4:-1]
        except ValueError:
            print("ce choix n'est pas possible. Entrez 1, 2 ou 3")

def niveau_ordi(niv):
    if niv == "naïf":
        if len(allumettes[0]) >= 3:
            nb_allum = randint(1,3)
        elif len(allumettes[0]) == 2:
            nb_allum = randint(1,2)
        else:
            nb_allum = 1
        print(nb_allum)
        retrait_allumettes(nb_allum)
    elif niv == "distrait":
        pass
    elif niv == "rapide":
        pass
    elif niv == "expert":
        pass

def deroulement_jeu(joueur1, joueur2, niv):
    if joueur1 == 'Ordinateur':
        while allumettes != [[], [], []]:
            print("{}, combien d'allumettes prenez-vous ? ".format(joueur1), end='')
            niveau_ordi(niv)
            affiche_ecran(allumettes)
            dernierjoueur = joueur2
            print()
            if allumettes != [[], [], []]:
                nb_allum = int(input("{}, combien d'allumettes prenez-vous ? ".format(joueur2)))
                retrait_allumettes(nb_allum)
                affiche_ecran(allumettes)
                dernierjoueur = joueur1
                print()
        else:
            print("{} a gagné !".format(dernierjoueur))
    else:
        while allumettes != [[], [], []]:
            nb_allum = int(input("{}, combien d'allumettes prenez-vous ? ".format(joueur1)))
            retrait_allumettes(nb_allum)
            affiche_ecran(allumettes)
            dernierjoueur = joueur2
            print()
            if allumettes != [[], [], []]:
                print("{}, combien d'allumettes prenez-vous ? ".format(joueur2), end='')
                niveau_ordi(niv)
                affiche_ecran(allumettes)
                dernierjoueur = joueur1
                print()
        else:
            print("{} a gagné !".format(dernierjoueur))  

def menu():
    print("Bienvenue dans le jeu des 13 allumettes dont les règles sont :")
    print("--- Il y a 13 allumettes disposées en rang sur la table")
    affiche_ecran(allumettes)
    print("--- A chaque tour de jeu, on peut retirer 1, 2 ou 3 allumettes")
    print("--- S'il ne reste plus qu'un seule allumette sur la table vous avez perdu !")
    nom_joueur = input("Quel est votre nom ? ")
    niveau = input("Niveau de l'ordinateur (n)aïf, (d)istrait, (r)apide ou (e)xpert ? ")
    if niveau.lower() == 'n': niv = 'naïf'
    elif niveau.lower() == 'd': niv = 'distrait'
    elif niveau.lower() == 'r': niv = 'rapide'
    else: niv = 'expert'
    print("Mon niveau est {}.".format(niv))
    qui_commence = input("Est-ce que vous commencez (o/n) ? ")
    if qui_commence == 'o':
        joueur1 = nom_joueur
        joueur2 = 'Ordinateur'
    else:
        joueur1 = 'Ordinateur'
        joueur2 = nom_joueur
    deroulement_jeu(joueur1, joueur2, niv)

menu()


